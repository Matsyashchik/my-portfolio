import React from "react";
import profile from '../../Images/IMG/3.png';
import t from "../../i18n/t";

import './Main.css';

function Main() {
    return (
        <main className="site-main">
            <h1 className="tittle"><b>{t("title.name")}</b>
                <br/>
                {t("title.status")}
            </h1>
            <img className="profile-photo" src={profile} alt="avatar"/>
        </main>
    );
}

export default Main;

