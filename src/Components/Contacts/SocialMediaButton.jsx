import React from "react";
import './SocialMediaButton.css';

export default function SocialMediaButton({id, title, href}) {
    return (
        <a className="sm-btn" href={href}>
            <i className={`fab fa-${title}`}></i>
        </a>
    );
}