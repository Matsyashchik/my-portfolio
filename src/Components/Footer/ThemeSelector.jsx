import React from "react";
import './ThemeSelector.css';

export default function ThemeSelector(props) {
    return (
        <label className="toggle-button">
            <input type="checkbox" defaultChecked={props.isDark} onClick={props.themeChanger} id="toggle-theme"
                   hidden/>
            <span className="switch-left">NIGHT</span>
            <span className="switch-right">DAY</span>
        </label>
    );
}