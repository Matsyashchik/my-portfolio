import React, {useState} from "react";
import Loader from "../Components/Loader/Loader";

const UseLoader = initialState => {
    const [visible, setVisible] = useState(initialState);

    const showLoader = () => setVisible(true);
    const hideLoader = () => setVisible(false);
    const loader = visible ? <Loader /> : null;

    return [loader, showLoader, hideLoader];

};

export default UseLoader;