import React from "react";
import './Header.css';
import {NavLink} from "react-router-dom";
import {useLocation} from "react-router-dom";
import t from "../../i18n/t";

export default function Header(props) {
    const location = useLocation();
    let elementClassName = location.pathname === "/" ? "page-header vertical" : "page-header horizontal";
    return (
        <header className={elementClassName}>
            <p className="logo"><NavLink to="/">VM</NavLink></p>
            <nav className="main-nav">
                <ul className="main-menu">
                    <li className="menu-item"><NavLink to="/About">{t("nav.about")}</NavLink></li>
                    <li className="menu-item"><NavLink to="/Portfolio">{t("nav.portfolio")}</NavLink></li>
                    <li className="menu-item"><NavLink to="/Hobby">{t("nav.hobby")}</NavLink></li>
                    <li className="menu-item"><NavLink to="/Contacts">{t("nav.contacts")}</NavLink></li>
                </ul>
            </nav>
        </header>
    );
}