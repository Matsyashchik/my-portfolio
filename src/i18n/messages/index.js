import ru from './ru-RU';
import en from './en-UK';

export default {
    ...ru,
    ...en,
}