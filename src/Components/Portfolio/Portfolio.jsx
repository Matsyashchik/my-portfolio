import React, {useCallback, useEffect, useState} from "react";
import './Portfolio.css';
import Project from "./Project";
import xmas from '../../Images/IMG/XMAS.PNG'
import {useHttp} from "../../hooks/useHttp";
import Loader from "../Loader/Loader";

export default function Portfolio(props) {
    const [projects, setProjects] = useState([]);
    const {loading, request} = useHttp();

    const fetchProjects = useCallback(async () => {
        try {
            const projects = await request('api/project/', 'GET');
            console.log(projects);
            const projectElements = projects.map(p => <Project key={p._id} title={p.title} date={p.date}
                                                              description={p.description}
                                                              img={xmas} /> );
            setProjects(projectElements);
        } catch (e) {
        }
    }, [request]);

    useEffect(() => {
        fetchProjects()
    }, [fetchProjects]);

    if (loading) {
        return <Loader/>
    }

    return (
        <>
            {!loading && <main className="page-portfolio">{projects}</main>}
        </>
    );
}