import React, {useState} from "react";
import './Loader.css'

const Loader = () => {
    return (
        <div className="loader">
            <div className="obj"/>
            <div className="obj"/>
            <div className="obj"/>
            <div className="obj"/>
            <div className="obj"/>
            <div className="obj"/>
            <div className="obj"/>
        </div>
    )
};

export default Loader;