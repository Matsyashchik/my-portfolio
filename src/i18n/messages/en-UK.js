import { LOCALES } from '../locales';

export default {
    [LOCALES.ENGLISH]: {
        "nav.about": "About me",
        "nav.portfolio": "Portfolio",
        "nav.hobby": "Hobby",
        "nav.contacts": "Contacts",
        "title.name": "Vladislav Matsyashchik",
        "title.status": "Frontend-developer",
        "copyright" : "Vladislav Matsyashchik. All rights reserved",
        "about.me" : "I am 21 years old and I am studying at the 3rd year of the university with a degree in Information Systems and Technologies.",
        "about.way" : "Having entered the specialty “World Economy” initially, over time I realized that my path through life is — IT. Having tried different directions and languages, I chose Frontend-development for myself for its visibility and universal development. I like to learn new things and improve in my field all the time.",
        "about.hobby" : "In my free time I have a couple of hobbies",
        "about.hobby.1" : "I play musical instruments, mainly guitar and write songs. In December 2019, he even recorded an album",
        "about.hobby.2" : "When I don’t write music, I’m listen and watch other artists and ",
        "about.hobby.2.link" : "creating playlists",
        "about.hobby.3" : "I am fond of basketball and long-distance running. Every year I take part in the largest half marathon in Russia with a synchronized start",
    }
}