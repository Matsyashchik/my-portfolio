import React from "react";
import './Project.css';
import {FormattedDate} from "react-intl";

export default function Project({title, date, img, description}) {
    return (
        <div className="project">
            <div className="project-info">
                <h6 className="project-title">{title}</h6>
                <p className="project-description">
                    <time className="project-date" datatime={date}><FormattedDate value={date} year="numeric"
                                                                                  month="long" day="2-digit"/></time>
                    <span>{description}</span>
                </p>
            </div>
            <img className="project-img" src={img} alt="project-img"/>
        </div>
    );
}