import React, {Suspense, useEffect} from "react";
import './Hobby.css';
// import Playlist from "./Playlist";
import Loader from "../Loader/Loader";
import useLoader from "../../hooks/useLoader";

const Playlist = React.lazy(
    () =>
        new Promise(resolve => {
            setTimeout(() => {
                resolve(import('./Playlist'))
            }, 1500)
        })
);

let playlists = [
    {
        id: 1,
        title: "Look Alive",
        href: "https://www.youtube.com/embed/videoseries?list=PLMrDPrUwhxz8O77zuZEADmetfrfndpHqk",
        description: "Плейлист с живыми выступлениями как российских так и иностранных исполнителей"
    },
    {
        id: 2,
        title: "TRIPaDROM",
        href: "https://www.youtube.com/embed/videoseries?list=PLMrDPrUwhxz-C8Mc7KM45nFx1LVVR_56K",
        description: "A$AP ROCKY, Tame Impala, Major Lazer, Jain and a lot of acid"
    },
    {
        id: 3,
        title: "Sensation Animation",
        href: "https://www.youtube.com/embed/videoseries?list=PLMrDPrUwhxz8Y6qHPTsyKliMOOM2ZcFBZ",
        description: "Мультики! Мультики! Мультики!!!"
    },
];

let playlistElements = playlists.map(p => <Playlist key={p.id} title={p.title} href={p.href}
                                                    description={p.description}/>);

export default function Hobby() {
    const [loader, showLoader, hideLoader] = useLoader(true);
    useEffect(hideLoader);    // cont load
    return (
        <main className="page-main">
                <section className="playlists">
                <Suspense fallback={<Loader/>}>
                    {playlistElements}
                </Suspense>
            </section>
        </main>
    );
}