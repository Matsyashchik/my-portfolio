import React from "react";
import './Contacts.css';
import SocialMediaButton from "./SocialMediaButton";


let socialMedias = [
    {id: 1, title: "gitlab", href: "https://gitlab.com/Matsyashchik"},
    {id: 2, title: "vk", href: "https://vk.com/matsyashchik"},
    {id: 3, title: "youtube", href: "https://www.youtube.com/user/CrazyDante914"},
    {id: 4, title: "soundcloud", href: "https://soundcloud.com/yghjchjz3nfl"},
    {id: 5, title: "instagram", href: "https://www.instagram.com/matsyashchik"},
];

let SocialMediaButtons = socialMedias.map(sm => <SocialMediaButton key={sm.id} title={sm.title} href={sm.href}/>);

export default function Contacts() {
    return (
        <main className="page-contacts">
            <div className="social-media">
                {SocialMediaButtons}
            </div>
        </main>
    );
}