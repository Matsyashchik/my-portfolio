import React from "react";
import './Playlist.css';

export default function Playlist({id, title, href, description}) {
    return (
        <div className="playlist">
            <div className="playlist-info">
                <h6 className="playlist-tittle"><a href={href}>{title}</a></h6>
                <p className="playlist-description">{description}</p>
            </div>
            <div className="playlist-video">
                <iframe title={id} src={href}
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowFullScreen/>
            </div>
        </div>
    );
}