import React, {Suspense} from 'react';
import {BrowserRouter, Route} from "react-router-dom";
import {I18nProvider, LOCALES} from "./i18n";

import Header from "./Components/Header/Header";
import Footer from './Components/Footer/Footer.jsx';
import Main from './Components/Main/Main';
import Hobby from "./Components/Hobby/Hobby";
import About from "./Components/About/About";
import Contacts from "./Components/Contacts/Contacts";
import Portfolio from "./Components/Portfolio/Portfolio";

import './App.css';
import Loader from "./Components/Loader/Loader";

let root = document.documentElement;
let isDark = false;
let lang = localStorage.getItem('lang');
if (lang === null) {
    lang = LOCALES.RUSSIAN;
}
let isRussian = lang === 'ru';

if (localStorage.getItem('theme')) {
    document.documentElement.setAttribute("theme", "dark");
    isDark = true;
}

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isDark: isDark,
            isRussian: isRussian,
            lang: lang,
        };
        this.changeLanguage = this.changeLanguage.bind(this);
    }

    render() {
        return (
            // <Hobby />
            <I18nProvider locale={this.state.lang}>
                <BrowserRouter basename='/'>
                    <Suspense fallback={<Loader />} >
                        <Route path='/' render={() => <Header/>}/>
                        <Route exact path='/'
                               render={() => <Main themeChanger={this.changeTheme} languageChanger={this.changeLanguage}
                                                   isDark={!this.state.isDark} isRussian={this.state.isRussian}/>}/>
                        <Route path='/About' render={() => <About/>}/>
                        <Route path='/Portfolio' render={() => <Portfolio/>}/>
                        <Route path='/Hobby' render={() => <Hobby/>}/>
                        <Route path='/Contacts' render={() => <Contacts/>}/>

                        <Route path='/'
                               render={() => <Footer themeChanger={this.changeTheme} languageChanger={this.changeLanguage}
                                                     isDark={!this.state.isDark} isRussian={this.state.isRussian}/>}/>
                    </Suspense>
                </BrowserRouter>
            </I18nProvider>
        )
    }

    changeTheme() {
        // const root = document.documentElement;
        console.log('change theme');

        if (root.hasAttribute("theme")) {
            root.removeAttribute("theme");
            delete localStorage.theme;
        } else {
            root.setAttribute("theme", "dark");
            localStorage.setItem('theme', 'dark')
        }
    }

    changeLanguage() {
        if (this.state.lang === LOCALES.RUSSIAN) {
            this.setState({"lang": LOCALES.ENGLISH});
            this.setState({"isRussian": true});
            root.setAttribute("lang", "en");
            localStorage.setItem('lang', 'en');
        } else {
            this.setState({"lang": LOCALES.RUSSIAN});
            this.setState({"isRussian": false});
            root.setAttribute("lang", "ru");
            localStorage.setItem('lang', 'ru');
        }
    }
}

export default App;