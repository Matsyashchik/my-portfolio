import React from "react";
import './LanguageSelector.css';

export default function LanguageSelector(props) {
    return (
        <label className="language-selector">
            <input type="checkbox" defaultChecked={props.isRussian} onClick={props.languageChanger}
                   id="language-change" hidden/>
            <span>RU</span>
            <b>{"  |  "}</b>
            <span>EN</span>
        </label>
    );
}