import React from "react";
import './Footer.css';
import LanguageSelector from "./LanguageSelector";
import ThemeSelector from "./ThemeSelector";
import t from "../../i18n/t";
import {useLocation} from "react-router-dom";


export default function Footer(props) {
    const location = useLocation();
    let elementClassName = location.pathname === "/" ? "page-footer vertical" : "page-footer";
    return (
        <footer className={elementClassName}>
            <LanguageSelector isRussian={props.isRussian} languageChanger={props.languageChanger}/>
            <span className="copyright">© 2020 {t('copyright')}</span>
            <ThemeSelector isDark={props.isDark} themeChanger={props.themeChanger}/>
        </footer>
    );
}