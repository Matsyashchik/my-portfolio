import React from "react";
import './About.css';
import runPhoto from '../../Images/IMG/onRun.jpg';
import workPhoto from '../../Images/IMG/onWork.jpg';
import t from "../../i18n/t";
import {NavLink} from "react-router-dom";

export default function About(props) {
    function getAge() {
        const age = (new Date().getTime() - new Date('1998-08-22')) / (24 * 3600 * 365.25 * 1000);
        return Math.floor(age);
    }

    return (
        <main className="page-about">
            <p className="paragraph">
                <img className="about-photo" src={workPhoto} alt="workPhoto"/>
                <span>{t('about.me', {num: getAge()})}</span>
                <br/>
                <span>{t('about.way')}</span>
            </p>
            {/*<img className="about-photo" src={workPhoto} alt="workPhoto"/>*/}
            <p className="paragraph">
                <img className="about-photo" src={runPhoto} alt="workPhoto"/>
                <span>{t('about.hobby')}</span>
                <br/>
                <ul className="hobby-list">
                    <li>{t('about.hobby.1')}</li>
                    <li>{t('about.hobby.2')}<NavLink to="/Hobby">{t("about.hobby.2.link")}</NavLink></li>
                    <li>{t('about.hobby.3')}</li>
                </ul>
            </p>

        </main>
    );
}